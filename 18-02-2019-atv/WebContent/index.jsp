<jsp:include page="menu.jsp"></jsp:include>
	<!-- slide -->
	<div class="slide" uk-slider="center:true;">
		<div class="uk-position-relative uk-visible-toggle uk-light">
			<div id="pc">Computadores</div>
			<ul class="uk-slider-items uk-grid uls">
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- slide3 -->
	<div class="slide">
		<div uk-slider="center:true; autoplay:true; autoplay-interval: 3000;">
			<div id="smart">smartphones</div>
			<ul class="uk-slider-items uk-grid uls">
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">alcatel</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
				<li>
					<div class="uk-card uk-card-default">
						<div class="uk-card-media-top">
							<img src="img/notdell.jpg" alt="notebook dell" class="img" />
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title">Notebook dell</h3>
							<p>
								R$ 2.549,00 <a uk-icon="icon: plus-circle" href="detalhes.jsp"
									uk-tooltip="title:ver detalhes do produto;"></a>
							</p>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
<jsp:include page="rodape.jsp"></jsp:include>
	