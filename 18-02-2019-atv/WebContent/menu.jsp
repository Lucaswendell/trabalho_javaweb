<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>HYPERTEC</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/uikit.min.css" />
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<div id="carregando">
		<div class="barLo">
			<div class="circleLo"></div>
			<p id="loading">loading</p>
		</div>
	</div>
	<!-- Menu -->
	<a class="uk-margin-right botaoMenu" uk-sticky="offset:0;"
		type="button" uk-toggle="#teste"><span
		uk-icon="icon: table;ratio:2;"></span></a>
	<div id="teste" uk-offcanvas>
		<div class="uk-offcanvas-bar uk-flex uk-flex-column">
			<button class="uk-offcanvas-close" uk-close></button>
			<ul
				class="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical">
				<li><a href="index.jsp"><span
						uk-icon="icon: home; ratio: 1.5;"></span> Home</a></li>
				<li class="uk-nav-divider"></li>
				<li><a href="formLogin.jsp"><span uk-icon="icon:sign-in; ratio: 1.5;"></span> Entrar</a></li>
				<li><a href="form.jsp">Cadastre-se</a></li>
				</li>
				<li class="uk-nav-divider"></li>
				<li class="uk-parent"><a href="#">Categorias</a>
					<ul class="uk-nav-sub">
						<li><a href="#">Sub item</a></li>
						<li><a href="#">Sub item</a></li>
					</ul></li>

			</ul>

		</div>
	</div>