<jsp:include page="menu.jsp"></jsp:include>
<div id="fundo" uk-grid>
	<!-- cadastrado -->
	<div>
		<form action='logar' method='post' class="form">
			<div class='uk-margin'>
				<div class='uk-inline'>
					<span class='uk-form-icon uk-form-icon-flip' uk-icon='icon: user'></span>
					<input class='uk-input' name='user' type='text'
						placeholder='usuario' id='usuario' required>
				</div>
			</div>

			<div class='uk-margin'>
				<div class='uk-inline'>
					<span class='uk-form-icon uk-form-icon-flip' uk-icon='icon: lock'></span>
					<input class='uk-input' name='pass' type='password'
						placeholder='senha' required>
				</div>
			</div>
			<button class='uk-button uk-button-default' type='submit'>Logar</button>
			<a href="cadastro.jsp" class="uk-link-reset uk-button-text">Primerio
				acesso</a>
		</form>
	</div>
	<!-- n�o cadastrado -->
		<div>
		<form action='logar' method='post' class="form">
			<div class='uk-margin'>
				<div class='uk-inline'>
					<span class='uk-form-icon uk-form-icon-flip' uk-icon='icon: user'></span>
					<input class='uk-input' name='user' type='text'
						placeholder='usuario' id='usuario' required>
				</div>
			</div>

			<div class='uk-margin'>
				<div class='uk-inline'>
					<span class='uk-form-icon uk-form-icon-flip' uk-icon='icon: lock'></span>
					<input class='uk-input' name='pass' type='password'
						placeholder='senha' required>
				</div>
			</div>
			<button class='uk-button uk-button-default uk-width-1-1' type='submit'>Cadastrar</button>
		</form>
	</div>
</div>
<jsp:include page="rodape.jsp"></jsp:include>
