package servlet;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.SQLException;
@WebServlet("/ServletTest")
public class ServletTest extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Resource(name="jdbc/project")
	private DataSource ds;
	Connection c = null;
	Statement s = null;
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			c = ds.getConnection(); //abri a conexao
			s = c.createStatement();
			String sql = "INSERT INTO livro (titulo, paginas, tema) VALUES ('teste_java_web',100,'qualquer');";
			s.execute(sql);
			c.close();
		}catch(SQLException e) {
			response.getWriter().print(e.getMessage());
		}
	}

}
