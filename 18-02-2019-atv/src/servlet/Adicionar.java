package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import controle.UsusarioControle;
import modelo.Usuario;

@WebServlet("/Adicionar")
public class Adicionar extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Adicionar() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/form.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			UsusarioControle user = new UsusarioControle();
			Usuario use = new Usuario();
			use.setNome(request.getParameter("nome"));
			use.setSenha(request.getParameter("senha"));
			use.setEmail(request.getParameter("email"));
			if(user.Insert(use)) {
				response.getWriter().print("<script>swal(\"deu certo doido\",\"sim\",success);</script>");
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		request.getRequestDispatcher("/form.jsp").forward(request, response);
	}
	
}
