package controle;

//imports das classes de conexao
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;

public final class Conexao {
	public Connection abrir() {
		Connection c = null;
		try {
			Class.forName("com.mysql.jdbc.Driver"); // driver de conexao que irei usar
			String banco = "java_web";
			String servidor = "jdbc:mysql://localhost/" + banco;
			String usr = "root";
			String pass = "root";
			c = DriverManager.getConnection(servidor, usr, pass); // pega conexao com o banco de dados
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return c;
	}

	public void fechar(Connection c) {
		try {
			c.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}
}
