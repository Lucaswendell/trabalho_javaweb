package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import controle.Conexao;

import modelo.Usuario;

public class UsusarioControle {
	public boolean Insert(Usuario u) {
		boolean resu = false;
		try {
			Connection c = new Conexao().abrir(); // abrir conexao
			PreparedStatement pre = c.prepareStatement("INSERT INTO user (nome, senha) VALUES (?,?);");
			pre.setString(1, u.getNome());
			pre.setString(2, u.getSenha());
			if (!pre.execute()) {
				resu = true;
			}
			new Conexao().fechar(c); // fechar conexao
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return resu;
	}
	public Usuario selecionar(int id) {
		Usuario user = null;
		try {
			Connection con = new Conexao().abrir();// abri conexao
			String sql = "select * from user where id=?";
			PreparedStatement ps = con.prepareStatement(sql); //preparando o comando para ser executado
			ps.setInt(1,id);
			ResultSet rs = ps.executeQuery(); //armazena dados recibidos do banco
			if(rs != null) {
				user.setNome(rs.getString("nome"));
				user.setSenha(rs.getString("senha"));
				user.setId(rs.getInt("id"));
			}
			new Conexao().fechar(con);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return user;
	}
}
